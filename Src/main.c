/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "bsp_uart.h"
#include "bsp_pwm.h"
#include "stdio.h"
#include "math.h"

#include <string.h>
 
#define RXBUFFERSIZE 256  
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
extern rc_info_t rc;
char buf[200];
unsigned int pwm_horizontal=3000,pwm_vertical=3000,pwm_vertical_reverse=3000; //1080~1920  +-20=1453~1547  +-15=1465~1535
unsigned int pwm_3508=1500;
uint16_t str_buf[50];
unsigned int MotorSpeed;  
int MotorOutput;
volatile uint8_t DMAflag = 0;
extern float angle_source[3];
extern float angle_source[3];
uint8_t RxBuffer[256];	
uint8_t aRxBuffer;
uint8_t Uart8_Rx_Cnt = 0;	

uint8_t flag=0;
uint32_t i;
float t_s,t_ms,t_us;
uint32_t cnt = 0;

uint8_t j_change_flag = 0;
uint8_t caipan[16];	
uint8_t caipanRxBuffer;
uint8_t Uart7_Rx_Cnt = 0;
uint8_t caipandata;
uint8_t caipanflag=0;
//static uint16_t cnt1 = 0;

// 判断裁判系统接收到多次00的计数单位
int time;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void HAL_GPIO_EXIT_Callback(uint16_t GPIO_Pin);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void PWM_SetDuty(TIM_HandleTypeDef *tim,uint32_t tim_channel,float duty){
	
	switch(tim_channel){	
		case TIM_CHANNEL_1: tim->Instance->CCR1 = (PWM_RESOLUTION*duty) - 1;break;
		case TIM_CHANNEL_2: tim->Instance->CCR2 = (PWM_RESOLUTION*duty) - 1;break;
		case TIM_CHANNEL_3: tim->Instance->CCR3 = (PWM_RESOLUTION*duty) - 1;break;
		case TIM_CHANNEL_4: tim->Instance->CCR4 = (PWM_RESOLUTION*duty) - 1;break;
	}
	
}

int fputc(int ch, FILE *f)
{      
	HAL_UART_Transmit(&huart6, (uint8_t *)&ch, 1, 0xffff);   
	return ch;
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  MX_TIM8_Init();
  MX_USART1_UART_Init();
  MX_USART6_UART_Init();
  MX_UART8_Init();
  MX_TIM10_Init();
  MX_TIM6_Init();
  MX_UART7_Init();
  /* USER CODE BEGIN 2 */
   led_off();
	 HAL_UART_Receive_IT(&huart8, (uint8_t *)&RxBuffer, 1);
	 HAL_UART_Receive_IT(&huart7, (uint8_t *)&caipanRxBuffer, 1);

	/* open dbus uart receive it */
	dbus_uart_init();
  HAL_GPIO_WritePin(GPIOH, POWER1_CTRL_Pin|POWER2_CTRL_Pin|POWER3_CTRL_Pin|POWER4_CTRL_Pin, GPIO_PIN_SET); // switch on 24v power
	/* Open 4 sets of PWM waves respectively */

	/**TIM4 GPIO Configuration    
	PD15     ------> TIM4_CH4
	PD14     ------> TIM4_CH3
	PD13     ------> TIM4_CH2
	PD12     ------> TIM4_CH1 
	*/
	HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_4);

	/**TIM5 GPIO Configuration    
	PI0     ------> TIM5_CH4
	PH12     ------> TIM5_CH3
	PH11     ------> TIM5_CH2
	PH10     ------> TIM5_CH1 
	*/

	HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_4);
	
	
	/**TIM8 GPIO Configuration    
	PI7     ------> TIM8_CH3
	PI6     ------> TIM8_CH2
	PI5     ------> TIM8_CH1
	PI2     ------> TIM8_CH4 
	*/
		
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_4);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

	__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, 1500);//3508
	HAL_TIM_Base_Start_IT(&htim10);
	
	/*装弹舵机控制6020 初始化 0° */
	__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_2, 2160);  // 6020 0 angle
		HAL_Delay(3000);
	
	/* 装弹程序  控制上下舵初始化  */
	/**TIM2 GPIO Configuration    
	PA1     ------> TIM2_CH2
	PA0     ------> TIM2_CH1
	PA2     ------> TIM2_CH3
	PA3     ------> TIM2_CH4 
	*/
	MX_TIM2_Init();
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_4);
	
	__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_2, 2275);  // num 2(2)
	__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_1, 2275);  // num 2(1)
	HAL_Delay(2000);
		
	/* 装弹舵机的四个舵机初始0° */
	__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_1, 1500);   // 3舵1飞   0°  夹子夹紧
	__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_4, 3000+150);  // 4舵2飞   0°  夹子夹紧
	__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_3, 1500);   // 5舵3飞   0°  夹子夹紧
	__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_4, 1500);   // 6舵4飞   0°  夹子夹紧
	while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
			
		/* 遥控器右 推杆 中间 手动装填飞镖  */
		if(rc.sw2==3)
		{
			__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_2, 2160+420);  // 6020
			while(rc.sw2==3) HAL_Delay(100);
			__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_2, 2160);  // 6020 0 angle
		}
		/* 手动程序 遥控器 右 推杆向上 左 推杆向上 手动控制 3508 6020 发射舵机  直流推杆 */
		/* 手动程序 遥控器 右 推杆向上 左 推杆向下 手动控制 3508 6020 发射舵机  直流推杆 */
		else if(rc.sw2==1)
		{
			if(rc.sw1==1)
			{
				if(DMAflag)
				{
					pwm_horizontal = pwm_horizontal - rc.ch1*0.026;  // 6020 control left and right
					pwm_vertical=1500-(-rc.ch2)*0.75;  // 右直流推杆
					pwm_vertical_reverse=1500-(-rc.ch2)*0.75;  // 左直流推杆
					pwm_3508=1500-(rc.ch4)/2;
					
					if(pwm_horizontal<2387) pwm_horizontal=2387;//6020
					else if(pwm_horizontal>3489) pwm_horizontal=3489; 
					
					if(pwm_vertical<1300) pwm_vertical=1300;  // 直流推杆电机
					else if(pwm_vertical>1920) pwm_vertical=1920;
					
					if(pwm_vertical_reverse<1300) pwm_vertical_reverse=1300;
					else if(pwm_vertical_reverse>1920) pwm_vertical_reverse=1920; 
					
					if(pwm_3508<1150) pwm_3508=1150;//3508
					else if(pwm_3508>1850) pwm_3508=1800;
						
					/*6020,rc.ch1,右摇杆水平*/
					
					__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_3, pwm_horizontal);
					DMAflag = 0;
				}

				// 手动
				__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, pwm_3508);//3508
				
				/*直流推杆电机,右摇杆垂直，rc.ch2*/
				if(rc.ch2==0)
				{
					HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4,GPIO_PIN_RESET);//IN2  PE4
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0,GPIO_PIN_RESET);//IN1  PB0
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4,GPIO_PIN_RESET);//IN4  PA4
					HAL_GPIO_WritePin(GPIOI, GPIO_PIN_9,GPIO_PIN_RESET);//IN3  PI9
				}
				
				else if(rc.ch2>0)
				{
					HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_0);
					HAL_GPIO_TogglePin(GPIOI, GPIO_PIN_9);//1010
					__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_2, pwm_vertical_reverse);
					__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_3, pwm_vertical); //0000
				}
				
				else if(rc.ch2<0)
				{
					HAL_GPIO_TogglePin(GPIOE, GPIO_PIN_4);
					HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_4);//0101
					__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_2, pwm_vertical_reverse);
					__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_3, pwm_vertical); //0000
				}
				/*num 1舵机ch3,左摇杆水平*/
				if(rc.ch3==660)  // 右
				{
					__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_1, 3000);//舵机 向上
				}
				else if(rc.ch3==-660)  // 左
				{
					 __HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_1, 5000);//PI5 向下
				}		
			}
			
			 /* 控制直流推杆电机根据设定自动寻找角度
			 3508 发射舵机的自动程序  在3508拉飞镖向下的时候碰到开关会立即停住然后向上走 
			 碰到上面的开关时，会立刻停止然后将发射舵机向下调整，打出飞镖	  pwm=5000*/
			
			
			else if(rc.sw1==2)    
			{
			/*保证开启自动程序的时候 两个控制高度的舵机1和2 都在上面 不压板子 */
			__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_2, 2275);  // num 2(2)
			__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_1, 2275);  // num 2(1)
//			HAL_Delay(2000);
			/* 保证开启自动程序的时候  6020 初始化0°  */
			__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_2, 2160);  // 6020 0 angle
//			HAL_Delay(1000);
			
			/*-30 是规定的角度*/
# define ANGLE -26
# define D 1
			while(angle_source[1] > ANGLE+D || angle_source[1] < ANGLE-D)
			{
				if(angle_source[1] < ANGLE-D)
				{
					HAL_GPIO_TogglePin(GPIOE, GPIO_PIN_4);
					HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_4);//0101
					__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_2, 1500);
					__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_3, 1500); //0000
				}
				else if(angle_source[1] > ANGLE+D)
				{
					HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_0);
					HAL_GPIO_TogglePin(GPIOI, GPIO_PIN_9);//1010
					__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_2, 1500);
					__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_3, 1500); //0000
				}					
			}
			
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4,GPIO_PIN_RESET);//IN2  PE4
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0,GPIO_PIN_RESET);//IN1  PB0
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4,GPIO_PIN_RESET);//IN4  PA4
			HAL_GPIO_WritePin(GPIOI, GPIO_PIN_9,GPIO_PIN_RESET);//IN3  PI9
			
			for(int i=1; i<=4; i++)
			{
				/* 3508将狗头向下拉到装弹的位置 */
				__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, 1800);// 3508往下
				__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_1, 3000);//舵机 PI5zhunbe
				
				/* 拉皮筋 */
				while(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_1)!=0);  // 当底部按键没按到时，阻塞，等待
				if(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_1)==0)  //0?1?//shangqu
				{
					__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, 1150);  // 3508反转
//				HAL_Delay(500);  // TODO 调试用，比赛时应调整所有delay
					
					/* 升降舵机将板子向下压  */
					__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_2, 1200);  // num 2(2)  舵机在下
					__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_1, 1200);  // num 2(1)
					HAL_Delay(700);  // 升降舵机在下面 等待一秒再安装飞镖 TODO等待一下
					
					/* 装弹 */
					if(i==3) __HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_1, 500);  // 3舵1飞   90°  夹子松开
					else if(i==4) __HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_4, 1000);  // 4舵2飞   90°  夹子松开
					else if(i==1) __HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_3, 500);  // 5舵3飞   90°  夹子松开
					else if(i==2) __HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_4, 500);  // 6舵4飞   90°  夹子松开
					HAL_Delay(700);  // 等待 飞镖安装好 然后再收回升降舵机
					
					/* 飞镖卡上去以后将升降舵机收回 */
					__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_2, 2275);  // num 2(2) 舵机在上
					__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_1, 2275);  // num 2(1)
				}
				
				/* 发射 */
				while(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5)!=0);  // 当顶部按键没按到时，阻塞，等待
				if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5)==0)  //0?1?shangdaole
				{
					__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, 1500);  // 3508停转
					HAL_Delay(2000);  // TODO 调试用：3508到顶之后停2s再发射 （测试，观察可不可以将延时删除）
					__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_1, 5000);//舵机 PI5 fashe
					HAL_Delay(1000);  // 不能发射完以后 立马转6020
				}
				
				/* 6020 控制角度的程序 可以在一个for循环里面解决 */
				if(i<=3)  __HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_2, 2160+i*420);  // 6020 
			}
		}
			
		/* （接收的数据 2 准备 0 发射） 最终比赛使用的程序 左 推杆向下 */
		else if(rc.sw1==3)
		{

			if(caipandata==1 && caipanflag != 1)  // 接收到的数据

			{
				caipanflag = 1;
				/*保证开启自动程序的时候 两个控制高度的舵机1和2 都在上面 不压板子 */
				__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_2, 2275);  // num 2(2)
				__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_1, 2275);  // num 2(1)
	//			HAL_Delay(2000);
				/* 保证开启自动程序的时候  6020 初始化0°  */
				__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_2, 2160);  // 6020 0 angle
	//			HAL_Delay(1000);
				

				/*-ANGLE 是规定的角度*/

//				while(angle_source[1] > ANGLE+D || angle_source[1] < ANGLE-D)
//				{
//					if(angle_source[1] < ANGLE-D)
//					{
//						HAL_GPIO_TogglePin(GPIOE, GPIO_PIN_4);
//						HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_4);//0101
//						__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_2, 1500);
//						__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_3, 1500); //0000
//					}
//					else if(angle_source[1] > ANGLE+D)
//					{
//						HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_0);
//						HAL_GPIO_TogglePin(GPIOI, GPIO_PIN_9);//1010
//						__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_2, 1500);
//						__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_3, 1500); //0000
//					}					
//				}

//				HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4,GPIO_PIN_RESET);//IN2  PE4
//				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0,GPIO_PIN_RESET);//IN1  PB0
//				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4,GPIO_PIN_RESET);//IN4  PA4
//				HAL_GPIO_WritePin(GPIOI, GPIO_PIN_9,GPIO_PIN_RESET);//IN3  PI9

				/* 3508将狗头向下拉到装弹的位置 */
					__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, 1850);// 3508往下
					__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_1, 3000);//舵机 PI5zhunbe
				
 
				
				/* 拉皮筋 */
				while(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_1)!=0) HAL_Delay(1);  // 当底部按键没按到时，阻塞，等待
				if(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_1)==0)  //0?1?//shangqu
					{
						__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, 1150);  // 3508反转
						/* 升降舵机向下 来装弹 */
						__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_2, 1200);  // num 2(2)  舵机在下
						__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_1, 1200);  // num 2(1)

						HAL_Delay(500);

						/* 5舵3飞的舵机松开装弹 */
				    __HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_3, 500);  // 5舵3飞   90°  夹子松开
						HAL_Delay(500);
						/* 3508停转，将升降舵机向上 */
						__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_2, 2275); // num 2(2) 舵机在上
						__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_1, 2275);  // num 2(1)
					}
				while(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5)!=0) HAL_Delay(1);  // 当顶部按键没按到时，阻塞，等待
					if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5)==0)  //0?1?shangdaole
					{
						__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, 1500);  // 3508停转
					}
			}
			else if(time == 20)  // 接收到的数据
			{
				caipanflag=0;
				/* 发射 */
			__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_1, 5000);//舵机 PI5 fashe
			HAL_Delay(1000);
					__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_2, 2160+1*420);  // 6020 
				HAL_Delay(1000);
				for(int i=2; i<=4; i++)
				{
					/* 3508将狗头向下拉到装弹的位置 */
					__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, 1850);// 3508往下
					__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_1, 3000);//舵机 PI5zhunbe
					
					/* 拉皮筋 */
					while(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_1)!=0);  // 当底部按键没按到时，阻塞，等待
					if(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_1)==0)  //0?1?//shangqu
					{
						__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, 1150);  // 3508反转
						HAL_Delay(200);  // TODO 调试用，比赛时应调整所有delay
						
						/* 升降舵机将板子向下压  */
						__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_2, 1200);  // num 2(2)  舵机在下
						__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_1, 1200);  // num 2(1)
						HAL_Delay(1000);
					
						/* 装弹 */
						if(i==3) __HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_1, 500);  // 3舵1飞   90°  夹子松开
						else if(i==4) __HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_4, 1000);  // 4舵2飞   90°  夹子松开
//						else if(i==1) __HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_3, 500);  // 5舵3飞   90°  夹子松开
						else if(i==2) __HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_4, 500);  // 6舵4飞   90°  夹子松开
						HAL_Delay(1000);
						
						/* 飞镖卡上去以后将升降舵机收回 */
						__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_2, 2275); // num 2(2) 舵机在上
						__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_1, 2275);  // num 2(1)
					}
					
					/* 发射 */
					while(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5)!=0);  // 当顶部按键没按到时，阻塞，等待
					if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5)==0)  //0?1?shangdaole
					{
						__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, 1500);  // 3508停转
						HAL_Delay(1000);  // TODO 调试用：3508到顶之后停4s再发射
						__HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_1, 5000);//舵机 PI5 fashe
						HAL_Delay(1000);
					}
					
					/* 6020 控制角度的程序 可以在一个for循环里面解决 */
					
					if(i<=3)  __HAL_TIM_SetCompare(&htim8, TIM_CHANNEL_2, 2160+i*420);  // 6020 
				}
				
				break;
			}


		}
	}
		
		/* 装弹舵机的全流程  右侧推杆在上面的时候 自动 */
	  /*6020=0°: 2160(+420)	3舵1飞    PH10     ------> TIM5_CH1*/
		/*6020=90°: 2580  		4舵2飞    PI2     ------> TIM8_CH4 */
		/*6020=180°: 3000 		5舵3飞    PA2     ------> TIM2_CH3 */
		/*6020=270°: 3420 		6舵4飞    PA3     ------> TIM2_CH4 */
	 		
/* 陀螺仪输出串口*/
//   printf("\r\nangle1=%f  angle2=%f   angle3=%f",angle_source[0],angle_source[1],angle_source[2]);
//	 HAL_Delay(10);




	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 6;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim == (&htim10))//每10ms运行一次
	{
		if(flag==0 && rc.sw1==2)
		{
			cnt++;
		}
		else if(flag==1 && rc.sw1==2)
		{
			cnt--;
		}  
	}
	if(htim == (&htim6))//每10ms运行一次
	{
		
	}
}

/* 陀螺仪 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    /* Prevent unused argument(s) compilation warning */
  UNUSED(huart);
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_UART_TxCpltCallback could be implemented in the user file
   */
	if(huart == &huart8)
	{
		if(Uart8_Rx_Cnt >= 12)  
		{
			Uart8_Rx_Cnt = 0;
			memset(RxBuffer,0x00,sizeof(RxBuffer));
			//HAL_UART_Transmit(&huart6, (uint8_t *)"no", 10,0xFFFF);
		}
		else
		{
			RxBuffer[Uart8_Rx_Cnt++] = aRxBuffer;  
			if (RxBuffer[0]!=0x55) 
			{
				Uart8_Rx_Cnt = 0;
			}
		}
		HAL_UART_Receive_IT(&huart8, (uint8_t *)&aRxBuffer, 1);
	}
	
	//caipan
	else if(huart == &huart7)
	{
		HAL_UART_Receive_IT(&huart7, (uint8_t *)&caipanRxBuffer, 1);
		if(caipanRxBuffer == 0xA5)
		{
			memset(caipan,0x00,sizeof(caipan));
			Uart7_Rx_Cnt = 0;
			caipan[Uart7_Rx_Cnt++] = 0xA5;
		}
		else
		{
				if(Uart7_Rx_Cnt > 0)
				{

//					if(Uart7_Rx_Cnt == 1 && caipanRxBuffer == 0x06)
//						caipan[Uart7_Rx_Cnt++] = 0x06;
//					else if(Uart7_Rx_Cnt == 2 && caipanRxBuffer == 0x00)
//						caipan[Uart7_Rx_Cnt++] = 0x00;
//					else if(Uart7_Rx_Cnt == 3 || Uart7_Rx_Cnt == 4)
//						caipan[Uart7_Rx_Cnt++] = caipanRxBuffer;

					if(Uart7_Rx_Cnt == 1 ||Uart7_Rx_Cnt == 2 ||Uart7_Rx_Cnt == 3 || Uart7_Rx_Cnt == 4)
						caipan[Uart7_Rx_Cnt++] = caipanRxBuffer;
					else if(Uart7_Rx_Cnt == 5 && caipanRxBuffer == 0x0A)
						caipan[Uart7_Rx_Cnt++] = caipanRxBuffer;
					else if(Uart7_Rx_Cnt == 6 && caipanRxBuffer == 0x02)
						caipan[Uart7_Rx_Cnt++] = caipanRxBuffer;  // 开始读取第八位的数据
					else if(Uart7_Rx_Cnt == 7)
						caipan[Uart7_Rx_Cnt++] = caipanRxBuffer;
					else if(Uart7_Rx_Cnt == 8)
						caipan[Uart7_Rx_Cnt++] = caipanRxBuffer;

				else
				{
					Uart7_Rx_Cnt = 0;
					memset(caipan,0x00,sizeof(caipan));
				}
			

					if(Uart7_Rx_Cnt == 9)
					{
						caipandata = caipanRxBuffer;
						
						if(caipandata == 1 && j_change_flag == 0)
						{
							j_change_flag = 1;
						}
						else if(caipandata == 0 && j_change_flag == 1)  // 如果接收到的数据是0x01，将time+1
						{
							time++;
							
						}
						else if((caipandata == 2 || caipandata == 1) && time!= 0)  // 如果接收到的数据为0或者1，且time次数小于等于3，一定是接收到的数据有问题
						{
							time=0;
						}

					}
//					else if((caipandata == 2 || caipandata == 1) && time!= 0)  // 如果接收到的数据为0或者1，且time次数小于等于3，一定是接收到的数据有问题
//					{
//						time=0;
//					}
				}
			}

		}
	}
	

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
